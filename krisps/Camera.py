from picamera import PiCamera
from picamera.array import PiRGBArray

class Camera():
    def __init__():
        camera = PiCamera()
        camera.resolution = (640, 480)
        rawCapture = PiRGBArray(camera, size=(640, 480))

    def frames():
        return camera.capture_continuous(rawCapture,format="bgr",use_video_port=True)