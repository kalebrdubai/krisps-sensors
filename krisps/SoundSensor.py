from gpiozero import MCP3008

class SoundSensor:
	def __init__(self,pin):
		self.sound = MCP3008(channel=pin, device=0)
	
	def value(self):
 		return int(self.sound.value*1000)
