from gpiozero import HoldMixin, DigitalInputDevice

class TiltSwitch(HoldMixin, DigitalInputDevice):
    def __init__(
            self, pin=None, pull_up=False, active_state=None, bounce_time=None,
            hold_time=1, hold_repeat=False, pin_factory=None):
        super(TiltSwitch, self).__init__(
            pin, pull_up=pull_up, active_state=active_state,
            bounce_time=bounce_time, pin_factory=pin_factory)
        self.hold_time = hold_time
        self.hold_repeat = hold_repeat

    @property
    def value(self):
        """
        Returns 1 if the button is currently pressed, and 0 if it is not.
        """
        return super(TiltSwitch, self).value


TiltSwitch.is_pressed = TiltSwitch.is_active
TiltSwitch.pressed_time = TiltSwitch.active_time
TiltSwitch.when_pressed = TiltSwitch.when_activated
TiltSwitch.when_released = TiltSwitch.when_deactivated
TiltSwitch.wait_for_press = TiltSwitch.wait_for_active
TiltSwitch.wait_for_release = TiltSwitch.wait_for_inactive
