from gpiozero import AngularServo
from gpiozero.pins.pigpio import PiGPIOFactory

class AngleServo:
    def __init__(self,pin = None):
        self.a_servo = AngularServo(pin, pin_factory = PiGPIOFactory(), min_angle=-90, max_angle=90 , min_pulse_width=0.3/1000, max_pulse_width=2.3/1000)

    def angle(self, angle):
        self.a_servo.angle = angle

    def stop(self):
        self.a_servo.detach() 