import os
from subprocess import Popen, PIPE, DEVNULL
import time
import signal

class AudioPlayer:
    def __init__(self):
        print("AudioPlayer")
        self.soundInstance = None
       
    def play(self, trackName):
        if self.soundInstance != None:
            if self.soundInstance.poll() == None:
              os.killpg(self.soundInstance.pid, signal.SIGTERM)
              time.sleep(0.1)
        self.soundInstance =  Popen(["omxplayer", "-o","local", "/opt/kalebr-pi-agent/sounds/" + trackName + ".mp3"], stdin=PIPE, stdout=DEVNULL, preexec_fn=os.setsid)
        #self.soundInstance.stdin.write(b"q")
        
    def play_url(self, url):
        if self.soundInstance != None:
            if self.soundInstance.poll() == None:
              os.killpg(self.soundInstance.pid, signal.SIGTERM)
              time.sleep(0.1)
        self.soundInstance =  Popen(["omxplayer", "-o","local", "" + url + ""], stdin=PIPE, stdout=DEVNULL, preexec_fn=os.setsid)
        #self.soundInstance.stdin.write(b"q")
    

 

'''
player =  AudioPlayer()

player.play("b")

time.sleep(4)

player.play("a")

time.sleep(1)

player.play_url("https://file-examples.com/wp-content/uploads/2017/11/file_example_MP3_1MG.mp3")

'''
