import board
import neopixel

class NeoPixel:
    def __init__(self, pin = None, pixels = None):
        self.neo = neopixel.NeoPixel(board.D12, pixels)

    def fill(self, color):
        self.neo.fill(self.hex_to_rgb(color))
        
    def pixel(self, pixel, color):
        self.neo[pixel] = self.hex_to_rgb(color)
        
    def show(self):
        self.neo.show() 
        
    def off_all(self): 
        self.neo.fill((0, 0, 0))  
        
    def hex_to_rgb(self, value):
        value = value.lstrip('#')
        lv = len(value)
        return tuple(int(value[i:i+lv//3], 16) for i in range(0, lv, lv//3))

    
        
'''     
    from krisps import NeoPixel
    import time

    neo = NeoPixel(12, 4)
    neo.fill("#f9c700")
    time.sleep(1)

    neo.off_all()
    time.sleep(1)

    neo.pixel(0, "#2717c4")
    time.sleep(1)

    neo.off_all() 
'''  
