import RPi.GPIO as GPIO

class MagneticSwitch:

	def __init__(self,pin):
		self.pin = pin
		GPIO.setmode(GPIO.BCM)
		GPIO.setup(pin, GPIO.IN)
		
	def getValue(self):
		return GPIO.input(self.pin)
		
