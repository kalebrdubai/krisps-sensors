from gpiozero import MCP3008

class TemperatureSensor:
	def __init__(self, pin=None):
		self.tmp = MCP3008(channel=pin, device=0)
			
	def value(self, units='degrees'):
		temperature = (self.tmp.value * 3.3 - 0.25)*10
		 
		if units == 'fahrenheit':
			fahrenheit = temperature*9.0/5 + 32
			return round(fahrenheit, 2)
			
		else:
			return round(temperature, 2)

