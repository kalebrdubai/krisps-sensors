import evdev
from evdev import InputDevice, categorize, ecodes


class LogitecGamepad:
    def __init__(self, pin=None):
        current_device = '/dev/input/event0'
        devices = [evdev.InputDevice(path) for path in evdev.list_devices()]
        for device in devices:
            if device.name == 'Logitech Gamepad F710':
                current_device = device.path

        self.pad = evdev.InputDevice(current_device)

    def read_loop(self):
        return self.pad.read_loop()

    def button_pressed(self, key, event):
        switcher = {
            "Y": event.code == 308 and event.type == 1 and event.value == 1,
            "B": event.code == 305 and event.type == 1 and event.value == 1,
            "A": event.code == 304 and event.type == 1 and event.value == 1,
            "X": event.code == 307 and event.type == 1 and event.value == 1,
            "RB": event.code == 311 and event.type == 1 and event.value == 1,
            "LB": event.code == 310 and event.type == 1 and event.value == 1,
            "BACK": event.code == 314 and event.type == 1 and event.value == 1,
            "START": event.code == 315 and event.type == 1 and event.value == 1,
            "PAD-UP": event.code == 17 and event.type == 3 and event.value == -1,
            "PAD-DOWN": event.code == 17 and event.type == 3 and event.value == 1,
            "PAD-LEFT": event.code == 16 and event.type == 3 and event.value == -1,
            "PAD-RIGHT": event.code == 16 and event.type == 3 and event.value == 1,
            "THUMB-LEFT-UP": event.code == 1 and event.type == 3 and (event.value // 129) < 1,
            "THUMB-LEFT-DOWN": event.code == 1 and event.type == 3 and (event.value // 129) > 1,
            "THUMB-LEFT-LEFT": event.code == 0 and event.type == 3 and (event.value // 129) < 0,
            "THUMB-LEFT-RIGHT": event.code == 0 and event.type == 3 and (event.value // 129) > 0,
            "LT": event.code == 2 and event.type == 3 and event.value >= 0,
            "RT": event.code == 5 and event.type == 3 and event.value >= 0,
            "THUMB-RIGHT-UP": event.code == 4 and event.type == 3 and (event.value // 129) <= 0,
            "THUMB-RIGHT-DOWN": event.code == 4 and event.type == 3 and (event.value // 129) > 1,
            "THUMB-RIGHT-LEFT": event.code == 3 and event.type == 3 and (event.value // 129) < 0,
            "THUMB-RIGHT-RIGHT": event.code == 3 and event.type == 3 and (event.value // 129) > 0
        }

        return switcher.get(key, False)

    def value(self, event):
        if (event.code == 1 or event.code == 4 or event.code == 0 or event.code == 3):
            if (event.value == 255):
                return 0
            elif (event.value > 255):
                return event.value // 255
            elif (event.value < 255):
                return -(event.value // 255)
        else:
            if (event.value == 0):
                return event.value
            elif (event.value > 0):
                return event.value
            elif (event.value < 0):
                return -(event.value)


# Initiate LogitecGamepad
'''
gamepad = LogitecGamepad()

for event in gamepad.read_loop():
    if gamepad.button_pressed("1", event):
        print("1")

    if gamepad.button_pressed("2", event):
        print("2")

    if gamepad.button_pressed("3", event):
        print("3")

    if gamepad.button_pressed("4", event):
        print("4")

    if gamepad.button_pressed("RB", event):
        print("RB")

    if gamepad.button_pressed("LB", event):
        print("LB")

    if gamepad.button_pressed("BACK", event):
        print("BACK")

    if gamepad.button_pressed("START", event):
        print("START")

    if gamepad.button_pressed("PAD-UP", event):
        print("PAD-UP")

    if gamepad.button_pressed("PAD-DOWN", event):
        print("PAD-DOWN")

    if gamepad.button_pressed("PAD-LEFT", event):
        print("PAD-LEFT")

    if gamepad.button_pressed("PAD-RIGHT", event):
        print("PAD-RIGHT")

    if gamepad.button_pressed("THUMB-LEFT-UP", event):
        print("THUMB-LEFT-UP")

    if gamepad.button_pressed("THUMB-LEFT-DOWN", event):
        print("THUMB-LEFT-DOWN")

    if gamepad.button_pressed("THUMB-LEFT-LEFT", event):
        print("THUMB-LEFT-LEFT")

    if gamepad.button_pressed("THUMB-LEFT-RIGHT", event):
        print("THUMB-LEFT-RIGHT")

    if gamepad.button_pressed("LT", event):
        print("LT")
        print(gamepad.value(event))

    if gamepad.button_pressed("RT", event):
        print("RT")
        print(gamepad.value(event))

    if gamepad.button_pressed("THUMB-RIGHT-UP", event):
        print("THUMB-RIGHT-UP")

    if gamepad.button_pressed("THUMB-RIGHT-DOWN", event):
        print("THUMB-RIGHT-DOWN")

    if gamepad.button_pressed("THUMB-RIGHT-LEFT", event):
        print("THUMB-RIGHT-LEFT")

    if gamepad.button_pressed("THUMB-RIGHT-RIGHT", event):
        print("THUMB-RIGHT-RIGHT")

'''
