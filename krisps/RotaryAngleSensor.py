from gpiozero import MCP3008

class RotaryAngleSensor:
	def __init__(self,pin):
		self.rotary_angle = MCP3008(channel=pin, device=0)
	
	def value(self):
 		return int(self.rotary_angle.value*1000)

