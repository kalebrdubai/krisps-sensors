import board
import busio
import adafruit_adxl34x


class Accelerometer:
    def __init__(self):
        self.i2c = busio.I2C(board.SCL, board.SDA)
        self.acc = adafruit_adxl34x.ADXL343(self.i2c)
        #self.acc = adafruit_adxl34x.ADXL343(self.i2c, address =  0x77)
        
    def value(self, axis):
        return self.acc.acceleration[axis]
  
       
'''
     
acc = Accelerometer()

while True:
    print(acc.value(0))

'''    

     
