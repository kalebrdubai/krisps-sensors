import board
import busio
import adafruit_tcs34725

class ColorSensor:
    def __init__(self):
        self.color = adafruit_tcs34725.TCS34725(busio.I2C(board.SCL, board.SDA))

    def rgb(self):
        return self.color.color_rgb_bytes
        
    def hex(self):
        return '%02x%02x%02x' % self.color.color_rgb_bytes


