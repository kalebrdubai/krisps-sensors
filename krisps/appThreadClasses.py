import threading
import time
import traceback
import subprocess

def handleExceptions(traceback):
  tb = traceback.format_exc()
  subprocess.call(["echo", tb])

class StoppableThread(threading.Thread):
    """Thread class with a stop() method. The thread itself has to check
    regularly for the stopped() condition."""
    def __init__(self):
        super(StoppableThread, self).__init__()
        self._stop_event = threading.Event()
    def stop(self):
        self._stop_event.set()
    def stopped(self):
        return self._stop_event.is_set()

class ReadVariables(StoppableThread):
  def __init__(self, name, client, readInputPins, pub_channel):
    super(ReadVariables, self).__init__()
    self.client = client
    self.readInputPins = readInputPins
    self.pub_channel = pub_channel
    self.name = name
  def run(self):
    print ("Starting" + self.name)
    while not self.stopped():
        try:
            self.client.publish(self.pub_channel, self.readInputPins())
        except:
            handleExceptions(traceback)
        time.sleep(0.5)
    print ("Closing Thread: " + self.name)

class MqttServiceLoop (StoppableThread):
  def __init__(self, name, client):
    super(MqttServiceLoop, self).__init__()
    self.client = client
    self.name = name
  def run(self):
    print ("Starting" + self.name)
    while not self.stopped():
      self.client.loop()
      time.sleep(0.05)
    print ("Closing Thread: " + self.name)

class MessageEventThread (StoppableThread):
  def __init__(self, name, mainFunction, message):
    super(MessageEventThread, self).__init__()
    self.name = name
    self.mainFunction = mainFunction
    self.message = message
  def run(self):
    try:
        print ("Starting" + self.name)
        self.mainFunction(self, self.message)
        print ("Closing Thread: " + self.name)
    except:
        handleExceptions(traceback)

class MainCodeThread (StoppableThread):
  def __init__(self, name, mainFunction):
    super(MainCodeThread, self).__init__()
    self.name = name
    self.mainFunction = mainFunction
  def run(self):
    print ("Starting" + self.name)
    self.mainFunction(self)
    print ("Closing Thread: " + self.name)

class CustomThread (StoppableThread):
  def __init__(self, name, mainFunction):
    super(CustomThread, self).__init__()
    self.name = name
    self.mainFunction = mainFunction
  def run(self):
    print ("Starting" + self.name)
    self.mainFunction(self)
    print ("Closing Thread: " + self.name)
