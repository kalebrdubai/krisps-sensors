import board
from adafruit_ht16k33.matrix import Matrix8x8
import time

class LedMatrix:
    def __init__(self):
        self.matrix = Matrix8x8(board.I2C())

    def fill(self):
        self.matrix.fill(1)
    
    def shift(self, h, v, *a):
        self.matrix.shift(h, v, a)  
          
    def shift_up(self):
        self.matrix.shift_up()
    
    def shift_left(self):
        self.matrix.shift_left() 
        
    def shift_down(self):
        self.matrix.shift_down()
    
    def shift_right(self):
        self.matrix.shift_right() 
    
    def on(self, x, y):
        self.matrix[x, y] = 1
        
    def off(self, x, y):
        self.matrix[x, y] = 0
        
    def off_all(self):
        self.matrix.fill(0)   
        
    def draw_pattern(self, mat):
        for x in range(8):
            for y in range(8):
                self.matrix.pixel(x, y, mat[x][y])
          

'''
    from krisps import LedMatrix
    m  = LedMatrix()
    m.fill()
    m.shift(2, 0)
    m.shift_right()
    m.shift_left()

    m.draw_pattern([[1,0,0,0,0,0,0,1],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0]
,[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0]
,[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0]])

'''
