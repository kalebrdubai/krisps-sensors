import cv2

class USBCamera:
    def __init__():
        camera = cv2.VideoCapture(0)
        camera.resolution = (640, 480)

    def status():
        return camera.isOpened()

    def value():
        ret, frame = camera.read()
        return ret