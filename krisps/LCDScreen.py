import time
import board
import busio
import adafruit_character_lcd.character_lcd_i2c as character_lcd

class LCDScreen:
	def __init__(self):
		self.i2c = busio.I2C(board.SCL, board.SDA)
		self.lcd = character_lcd.Character_LCD_I2C(self.i2c, 16, 2)
		self.lcd.cursor = True
		
	def clear(self):
		self.lcd.clear()
		
	def message(self,msg):
		self.lcd.message = (str(msg))
		
	def set_cursor(self,column, row):
		self.lcd.cursor_position(column, row)
