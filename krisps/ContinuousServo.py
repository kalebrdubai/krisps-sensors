from gpiozero import Servo
from gpiozero.pins.pigpio import PiGPIOFactory

class ContinuousServo:
    def __init__(self,pin = None):
        self.servo = Servo(pin, pin_factory = PiGPIOFactory())

    def anticlockwise(self, speed):
        self.servo.value = speed
        
    def clockwise(self, speed):
        self.servo.value = -speed
        
    def stop(self):
        self.servo.detach() 


"""
    c = ContinuousServos(14)

    while True:    
        c.anticlockwise(0.5)
        time.sleep(5)
        c.clockwise(0.5)
        time.sleep(5)    
        c.stop()    
        time.sleep(5)
"""
