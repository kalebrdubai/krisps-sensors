from gpiozero import MCP3008

class SlidePotentiometer:
	def __init__(self,pin):
		self.slide = MCP3008(channel=pin, device=0)
	
	def value(self):
 		return int(self.slide.value*1000)

