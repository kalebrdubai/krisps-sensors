import paho.mqtt.client as mqtt
import json

# TODO: this code need to move for helper
try:
    file = open("/opt/kalebr-pi-agent/key.txt", "r")
    device_key = file.read().strip()
    file.close()
except:
    device_key = "demo"
    print("Something went wrong when writing to the file")

# device_key = "demo"

class Connect(mqtt.Client):

    def on_connect(self, mqttc, obj, flags, rc):
        print("connected: " + str(rc))

    def on_message(self, mqttc, obj, msg):
        print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))
        try:
            payload = json.loads(str(msg.payload.decode("utf-8", "ignore")))
            type = ""
            if "type" in payload:
                type = payload["type"]
            if type == "data":
                return
            if type == "comment":
                self.on_comment(payload["msg"])
            if type == "share":
                self.on_share()
            if type == "like":
                self.on_like()
            if type == "generalConnect":
                self.on_device_message(payload["senderDeviceKey"], payload["msg"])

        except  Exception as e:
            print(e)

    def send_to_chat(self, message):
        self.publish("deviceReadings/" + device_key + "/kalebr", payload='{"type":"message", "msg":"' + message + '"}')

    def send_to_chart(self, data):
        jsonStr = json.dumps({"id": "" + device_key + "", "type": "data", "val": data })
        self.publish("deviceReadings/" + device_key + "/kalebr", payload=jsonStr)

    def send_message_to_device(self, device, key, value):
        json = '{"type":"generalConnect", "senderDeviceKey":"' + device_key + '", "msg":{"' + key + '":"' + value + '"}}'
        self.publish("deviceMessage/" + device + "/kalebr", payload=json)

    def run(self):
        self.connect("mqtt-https.kalebr.com", 1883, 60)
        self.subscribe("deviceMessage/"+device_key+"/kalebr")
        self.username_pw_set("kalebr", "50364459")
        self.loop_start()


'''
connect = Connect()
connect.run()

def on_like():
    print("Thanks for the like")

def on_comment(msg):
    print(msg)
    connect.send_to_chat("Thanks for the comment")

def on_share():
    print("Thanks for share") 

def on_device_message(sender, msg):
    print(sender)    
    print(msg["water"])    

while True:
    time.sleep(.5)
    connect.send_massage_to_device("demo", "water","on")
    connect.send_to_chat("hi")
'''