import RPi.GPIO as GPIO
import time

class MotorSpeedSensor:
    def __init__(self, pin):
        self.pin = pin
        self.r_count = 0
        self.total_count = 0
        self.r_rpm = 0
        self.start = 0
        self.end = 0
        self.delta = 0
        self.sample = 100
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.pin, GPIO.IN)
        GPIO.setup(self.pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.add_event_detect(pin, GPIO.RISING, callback=self.motor_callback)

    def set_start(self):
        self.start = time.time()

    def set_end(self):
        self.end = time.time()

    def rpm(self):
        return self.r_rpm

    def count(self):
        return self.total_count

    def motor_callback(self, channel):
        if self.r_count == 0:
            self.set_start()
            self.r_count += 1
            self.total_count += 1
        else:
            self.r_count += 1
            self.total_count += 1

        if self.r_count >= self.sample:
            self.set_end()
            self.delta = self.end - self.start
            self.delta = self.delta / 60
            self.r_rpm = (self.sample / self.delta)
            self.r_count = 0

'''
    ir_sensor_4 = MotorSpeedSensor(4)
    
    # Main Code
    if __name__ == "__main__":
    while True:
        time.sleep(0.0001)
        print(ir_sensor_4.count())
        print(ir_sensor_4.rpm())
'''

