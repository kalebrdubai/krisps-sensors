from gpiozero import HoldMixin, DigitalInputDevice

class TouchSensor(HoldMixin, DigitalInputDevice):
    def __init__(
            self, pin=None, pull_up=False, active_state=None, bounce_time=None,
            hold_time=1, hold_repeat=False, pin_factory=None):
        super(TouchSensor, self).__init__(
            pin, pull_up=pull_up, active_state=active_state,
            bounce_time=bounce_time, pin_factory=pin_factory)
        self.hold_time = hold_time
        self.hold_repeat = hold_repeat

    @property
    def value(self):
        """
        Returns 1 if the button is currently pressed, and 0 if it is not.
        """
        return super(TouchSensor, self).value


TouchSensor.is_pressed = TouchSensor.is_active
TouchSensor.pressed_time = TouchSensor.active_time
TouchSensor.when_pressed = TouchSensor.when_activated
TouchSensor.when_released = TouchSensor.when_deactivated
TouchSensor.wait_for_press = TouchSensor.wait_for_active
TouchSensor.wait_for_release = TouchSensor.wait_for_inactive
