from gpiozero import MCP3008

class MoistureSensor:
	def __init__(self,pin = None):
		self.moist = MCP3008(channel=pin, device=0)
	
	def value(self):
 		return int(self.moist.value*1000)
