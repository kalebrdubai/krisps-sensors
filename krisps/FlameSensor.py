from gpiozero import HoldMixin, DigitalInputDevice

class FlameSensor(HoldMixin, DigitalInputDevice):
    def __init__(
            self, pin=None, pull_up=False, active_state=None, bounce_time=None,
            hold_time=1, hold_repeat=False, pin_factory=None):
        super(FlameSensor, self).__init__(
            pin, pull_up=pull_up, active_state=active_state,
            bounce_time=bounce_time, pin_factory=pin_factory)
        self.hold_time = hold_time
        self.hold_repeat = hold_repeat

    @property
    def value(self):
        """
        Returns 1 if the button is currently pressed, and 0 if it is not.
        """
        return super(FlameSensor, self).value
        
FlameSensor.is_active = FlameSensor.is_active
