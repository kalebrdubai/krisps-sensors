import RPi.GPIO as GPIO

class MiniFan:
    def __init__(self,pin):
        self.pin = pin
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(pin, GPIO.OUT)
		
    def setValue(self,signal):
        return GPIO.output(self.pin,signal)
    
    def on(self):
        return GPIO.output(self.pin,GPIO.HIGH)
    
    def off(self):
        return GPIO.output(self.pin,GPIO.LOW)


