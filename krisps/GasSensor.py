from gpiozero import MCP3008

class GasSensor:
	def __init__(self,pin):
		self.gas = MCP3008(channel=pin, device=0)
	
	def value(self):
 		return int(self.gas.value*1000)
