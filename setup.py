from setuptools import setup


with open("README", 'r') as f:
    long_description = f.read()

setup(
    name='krisps',
    version='0.1.32',
    packages=['krisps'],
    long_description=long_description,
    url='',
    license='Krisps',
    author='Nilanga Saluwadana, Harsh Vardhan',
    author_email='n.sal@kalebr.com, h.vard@kalebr.com',
    description='Krisps Components',
    install_requires=['']
)
